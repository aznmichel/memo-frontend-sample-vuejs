# Memo FrontEnd (sample) - VueJS

This is a sample SPA (Single Page Application) that make Memos build in VueJS. 

The app uses an external REST API (built with Laravel 8 with tools such as Laravel Fortify and Laravel Passeport) that can be accessed at [Memoplace](https://memoplace-staging.herokuapp.com/). The source code for the API is available [here](https://gitlab.com/aznmichel/memo-api-sample-laravel).

The template used comes from the [Creative Tim - Vue Flutter theme](https://demos.creative-tim.com/vue-argon-design-system/?_ga=2.208154587.89462193.1606233208-1490611465.1606233208#/)

## Screenshots

|                                                Home Screen                                                 |                                                 Register Screen                                                  |                                                Login Screen                                                 |                                                  Memos Screen                                                  |                                                  Profile Screen                                                  |
| :---------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: |
| ![Home Screen](docs/home_screen.png?raw=true "Home") | ![Register Screen](docs/register_screen.png?raw=true "Register") | ![Login Screen](docs/login_screen.png?raw=true "Login") | ![Memos Screen](docs/memos_screen.png?raw=true "Memos") | ![Profile Screen](docs/profile_screen.png?raw=true "Profile") |

## Getting Started

### Prerequisites

- To run this project, you must have [npm](https://www.npmjs.com/) and [vue-cli](https://github.com/vuejs/vue-cli) (optionnal) installed in your machine.
- You must also set up the backend server for [Memoplace API](https://gitlab.com/aznmichel/memo-api-sample-laravel). You can find the instructions [here](https://gitlab.com/aznmichel/memo-api-sample-laravel).

### Project setup
After cloning the repository to your machine:
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Features

The app handles features like:

* Login
* Login with Social Media (in progress)
* Registration
* Add, Edit, Remove Memos
* View Home and Profile
* Log out

## Vue Packages

Some useful packages that I used:

* `axios` a great HTTP client library.
* `vuex` to manage app state.
* `vue-router` to manage route/view mapping.
* `vuelidate` to provide validation form.
* `vue-content-loader` to create placeholder loading, like Facebook cards loading.
* `vue2-transitions`  for reusable component transitions.

__*Note: The credentials for JWT Token is currently store in `localStorage`.*__

## Information

This Memo app is build for Mobile (Flutter) and Desktop (VueJS) usage. The source code for the Mobile app in Flutter is available [here](https://gitlab.com/aznmichel/memo-mobileapp-sample-flutter). 

![Memoplace Presentation](docs/memoplace_presentation.png?raw=true "memoplace_presentation")