import httpClient from '@/services/httpClient';
import store from '@/store';

export default class AccountService {

    init(tokens) {
        // Set tokens to localStorage
        localStorage.setItem("token", tokens.access_token);
        localStorage.setItem("refresh_token", tokens.refresh_token);
    }

    retrieveAccount() {
        console.log("RetrieveAccount...");
        store.commit('authenticate');
        return new Promise((resolve, reject) => {
            httpClient.get("api/user").then((response) => {
                resolve(response.data);
            }, error => {
                reject(error);
            })
        })
    }

    currentUser() {
        return store.getters.account;
    }

    authenticated() {
        return store.getters.authenticated;
    }
}