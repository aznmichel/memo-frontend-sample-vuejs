import axios from 'axios';
import { BASE_API_URL } from "@/variables";

// FIXME: experimental
export default class MemoService {
    retrieve() {
        return new Promise(resolve => {
            axios.get(BASE_API_URL).then(function (res) {
                resolve(res.data);
            });
        });
    }

    find(id) {
        return new Promise(resolve => {
            axios.get(`${BASE_API_URL}/${id}`).then(function (res) {
                resolve(res.data);
            });
        });
    }

    create(memo) {
        return new Promise(resolve => {
            axios.post(`${BASE_API_URL}`, memo).then(function (res) {
                resolve(res.data);
            });
        });
    }

    update(memo) {
        return new Promise(resolve => {
            axios.put(`${BASE_API_URL}/${id}`, memo).then(function (res) {
                resolve(res.data);
            });
        });
    }

    delete(memo) {
        return new Promise(resolve => {
            axios.put(`${BASE_API_URL}/${id}`, memo).then(function (res) {
                resolve(res);
            });
        });
    }
}