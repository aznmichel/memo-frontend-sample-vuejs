import axios from 'axios';
import { BASE_API_URL } from "@/variables";

const httpClient = () => {
    const defaultOptions = {
        baseURL: BASE_API_URL,
        headers: {
            'Content-Type': 'application/json',
        },
    };

    // Create instance
    let instance = axios.create(defaultOptions);

    // Set the AUTH token for any request
    instance.interceptors.request.use(function (config) {
        const token = localStorage.getItem('token');
        config.headers.Authorization = token ? `Bearer ${token}` : '';
        return config;
    }, error => {
        return Promise.reject(error);
    });

    return instance;
};

export default httpClient();
