import { ContentLoader } from "vue-content-loader";

export const SkeletonLoader = props => {
    return (
        <ContentLoader
            width={350}
            height={180}
            viewBox="0 0 350 180"
            backgroundColor="#f5f5f5"
            foregroundColor="#dbdbdb"
            {...props}
        >
            <rect x="4" y="8" rx="3" ry="3" width="5" height="146" />
            <rect x="5" y="150" rx="3" ry="3" width="100" height="5" />
            <rect x="100" y="9" rx="3" ry="3" width="5" height="146" />
            <rect x="5" y="8" rx="3" ry="3" width="100" height="5" />
            <circle cx="54" cy="40" r="18" />
            <rect x="35" y="79" rx="3" ry="3" width="50" height="4" />
            <rect x="35" y="69" rx="3" ry="3" width="50" height="4" />
            <circle cx="25" cy="80" r="5" />
            <rect x="35" y="89" rx="3" ry="3" width="50" height="4" />
            <rect x="35" y="119" rx="3" ry="3" width="50" height="4" />
            <rect x="35" y="109" rx="3" ry="3" width="50" height="4" />
            <circle cx="25" cy="120" r="5" />
            <rect x="35" y="129" rx="3" ry="3" width="50" height="4" />

            <rect x="124" y="8" rx="3" ry="3" width="5" height="146" />
            <rect x="125" y="150" rx="3" ry="3" width="100" height="5" />
            <rect x="220" y="9" rx="3" ry="3" width="5" height="146" />
            <rect x="125" y="8" rx="3" ry="3" width="100" height="5" />
            <circle cx="174" cy="40" r="18" />
            <rect x="155" y="79" rx="3" ry="3" width="50" height="4" />
            <rect x="155" y="69" rx="3" ry="3" width="50" height="4" />
            <circle cx="145" cy="80" r="5" />
            <rect x="155" y="89" rx="3" ry="3" width="50" height="4" />
            <rect x="155" y="119" rx="3" ry="3" width="50" height="4" />
            <rect x="155" y="109" rx="3" ry="3" width="50" height="4" />
            <circle cx="145" cy="120" r="5" />
            <rect x="155" y="129" rx="3" ry="3" width="50" height="4" />

            <rect x="244" y="8" rx="3" ry="3" width="5" height="146" />
            <rect x="245" y="150" rx="3" ry="3" width="100" height="5" />
            <rect x="340" y="9" rx="3" ry="3" width="5" height="146" />
            <rect x="245" y="8" rx="3" ry="3" width="100" height="5" />
            <circle cx="295" cy="40" r="18" />
            <rect x="275" y="79" rx="3" ry="3" width="50" height="4" />
            <rect x="275" y="69" rx="3" ry="3" width="50" height="4" />
            <circle cx="265" cy="80" r="5" />
            <rect x="275" y="89" rx="3" ry="3" width="50" height="4" />
            <rect x="275" y="119" rx="3" ry="3" width="50" height="4" />
            <rect x="275" y="109" rx="3" ry="3" width="50" height="4" />
            <circle cx="265" cy="120" r="5" />
            <rect x="275" y="129" rx="3" ry="3" width="50" height="4" />
        </ContentLoader>
    )
}
