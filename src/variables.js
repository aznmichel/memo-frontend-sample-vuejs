// variables.js
export const BASE_API_URL = 'https://memoplace-staging.herokuapp.com/'

export const MEMO_COLOR = {
    'primary': 'text-primary',
    'success': 'text-success',
    'warning': 'text-warning',
}