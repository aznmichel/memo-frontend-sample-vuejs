import Vue from "vue";
import Router from "vue-router";

// Components of header/footer view
import Header from "./views/app/core/fix/Header";
import Footer from "./views/app/core/fix/Footer";

// Components of content view
import Home from "./views/app/core/Home.vue";
import Register from "./views/app/auth/Register.vue";
import Login from "./views/app/auth/Login.vue";
import Profile from "./views/app/core/Profile.vue";

// Components of Argon view
import Components from "./views/Components.vue";

// Avoid NavigationDuplicated
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
};

Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        header: Header,
        default: Home,
        footer: Footer
      }
    },
    {
      path: "/login",
      name: "login",
      components: {
        header: Header,
        default: Login,
        footer: Footer
      }
    },
    {
      path: "/register",
      name: "register",
      components: {
        header: Header,
        default: Register,
        footer: Footer
      }
    },
    {
      path: "/profile",
      name: "profile",
      components: {
        header: Header,
        default: Profile,
        footer: Footer
      }
    },
    {
      path: "/doc",
      name: "components",
      components: {
        header: Header,
        default: Components,
        footer: Footer
      }
    },
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
