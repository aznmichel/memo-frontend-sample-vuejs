import Vue from "vue";
import Vuex from "vuex";
import { accountStore } from './modules/account-store';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
        accountStore
    }
});
export default store;