import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import Argon from "./plugins/argon-kit";

import router from "./router";
import store from './store';
import Vuelidate from 'vuelidate'

import AccountService from './services/api/accountService';

Vue.config.productionTip = false

Vue.use(Argon);
Vue.use(Vuelidate)

new Vue({
  render: h => h(App),
  store,
  router,
  provide: {
    accountService: () => new AccountService()
  },
}).$mount('#app')
